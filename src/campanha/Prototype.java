package campanha;

public interface Prototype<T> {
    T clone();
}