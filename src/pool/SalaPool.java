package pool;

import java.util.ArrayList;
import java.util.List;

public class SalaPool implements Pool<Sala> {
    private List<Sala> salas;

    public SalaPool() {
        this.salas = new ArrayList<>();
        this.salas.add(new Sala("sala1"));
        this.salas.add(new Sala("sala2"));
        this.salas.add(new Sala("sala3"));
    }

    public Sala acquire() {
        // escolhe uma sala da coleção
        if (this.salas.size() > 0) {
            return this.salas.remove(0);
        } else {
            return null;
        }
    }

    public void release(Sala sala) {
        // adiciona a sala na coleção
    }
}
