package boleto;

import java.util.Calendar;

public class ItauBoletoBuilder implements BoletoBuilder {
    @Override
    public void buildSacado(String sacado) {
        //regras
    }

    @Override
    public void buildCedente(String cedente) {
        //regras
    }

    @Override
    public void buildValor(double valor) {
        //regras
    }

    @Override
    public void buildVencimento(Calendar vencimento) {
        //regras
    }

    @Override
    public void buildNossoNumero(int nossoNumero) {
        //regras
    }

    @Override
    public void buildCodigoDeBarras() {
        //regras
    }

    @Override
    public void buildLogotipo() {
        //regras
    }

    @Override
    public Boleto getBoleto() {
        return null;
    }
    // implementação dos métodos seguindo as regras da FEBRABAN e do Itaú
}
