package visitor;

public interface AtualizadorDeFuncionario {
    void atualiza(Gerente g);

    void atualiza(Telefonista t);
}
