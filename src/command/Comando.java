package command;

public interface Comando {
    void executa(Player player);
}