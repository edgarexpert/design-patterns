package composite;

import java.util.ArrayList;
import java.util.List;

public class Caminho implements Trecho {
    private List<Trecho> trechos = new ArrayList<>();

    public void adiciona(Trecho trecho) {
        this.trechos.add(trecho);
    }

    public void remove(Trecho trecho) {
        this.trechos.remove(trecho);
    }

    public void imprime() {
        // imprime na tela as informações desse caminho.
    }
}
