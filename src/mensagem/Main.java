package mensagem;

import mensagem.emissor.*;

class Main {
    public static void main(String[] args) {

        Emissor emissorSMS = new EmissorSMS();
        emissorSMS.envia("K19 -Treinamentos");

        Emissor emissorEmail = new EmissorEmail();
        emissorEmail.envia("K19 -Treinamentos");

        Emissor emissorJMS = new EmissorJMS();
        emissorJMS.envia("K19 -Treinamentos");

        EmissorCreator creatorSMS = new EmissorAssincronoCreator();
        Emissor emissorSMS1 = creatorSMS.create(EmissorCreator.SMS);
        emissorSMS1.envia("K19 -Treinamentos");

        EmissorCreator creatorEmail = new EmissorAssincronoCreator();
        Emissor emissorEmail1 = creatorEmail.create(EmissorCreator.EMAIL);
        emissorEmail1.envia("K19 -Treinamentos");

        EmissorCreator creatorJMS = new EmissorSincronoCreator();
        Emissor emissorJMS1 = creatorJMS.create(EmissorCreator.JMS);
        emissorJMS1.envia("K19 -Treinamentos");

    }

}