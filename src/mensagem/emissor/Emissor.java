package mensagem.emissor;

public interface Emissor {
    void envia(String mensagem);
}