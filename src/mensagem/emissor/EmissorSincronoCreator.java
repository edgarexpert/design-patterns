package mensagem.emissor;

public class EmissorSincronoCreator extends EmissorCreator {
    public Emissor create(int tipoDeEmissor) {
        if (tipoDeEmissor == EmissorCreator.SMS) {
            //return new EmissorSincronoSMS();
            return new EmissorSMS();
        } else if (tipoDeEmissor == EmissorCreator.EMAIL) {
            //return new EmissorSincronoEmail();
            return new EmissorEmail();
        } else if (tipoDeEmissor == EmissorCreator.JMS) {
            //return new EmissorSincronoJMS();
            return new EmissorJMS();
        } else {
            throw new IllegalArgumentException("Tipo de mensagem.emissor não suportado");
        }
    }
}