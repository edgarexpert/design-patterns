package mensagem.emissor;

public class EmissorAssincronoCreator extends EmissorCreator {

    @Override
    public Emissor create(int tipoDeEmissor) {
        if (tipoDeEmissor == SMS) {
            //return new EmissorAssincronoSMS();
            return new EmissorJMS();
        } else if (tipoDeEmissor == EMAIL) {
            //return new EmissorAssincronoEmail();
            return new EmissorEmail();
        } else if (tipoDeEmissor == JMS) {
            //return new EmissorAssincronoJMS();
            return new EmissorJMS();
        } else {
            throw new IllegalArgumentException("Tipo de mensagem.emissor não suportado");
        }
    }
}