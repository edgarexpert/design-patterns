package mensagem.receptor;

public interface Receptor {
    String recebe();
}