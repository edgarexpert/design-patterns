package facade;

public class TestePedidoFacade {
    public static void main(String[] args) {
        Estoque estoque = new Estoque();
        Financeiro financeiro = new Financeiro();
        PosVenda posVenda = new PosVenda();

        /*Pedido p = new Pedido("", "", "");
        estoque.enviaProduto(p.getProduto(), p.getEnderecoDeEntrega(), p.getNotaFiscal());
        financeiro.fatura(p.getCliente(), p.getNotaFiscal());
        posVenda.agendaContato(p.getCliente(), p.getProduto());*/

        PedidoFacade facade = new PedidoFacade(estoque, financeiro, posVenda);
        Pedido pedido = new Pedido("Notebook", "Rafael Cosentino", "Av Brigadeiro Faria Lima, 1571, São Paulo, SP");
        facade.registraPedido(pedido);

        facade.cancelaPedido(pedido);

    }

}
