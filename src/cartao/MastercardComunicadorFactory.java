package cartao;

import mensagem.emissor.Emissor;
import mensagem.emissor.EmissorCreator;
import mensagem.receptor.Receptor;
import mensagem.receptor.ReceptorCreator;

public class MastercardComunicadorFactory implements ComunicadorFactory {
    private EmissorCreator emissorCreator = new EmissorCreator();
    private ReceptorCreator receptorCreator = new ReceptorCreator();

    public Emissor createEmissor() {
        return this.emissorCreator.create(EmissorCreator.MASTERCARD);
    }

    public Receptor createReceptor() {
        return this.receptorCreator.create(ReceptorCreator.MASTERCARD);
    }

//    public ComunicadorFactory getComunicadorFactory(Cartao cartao) {
//        String bandeira = cartao.getBandeira();
//        Class clazz = Class.forName(bandeira + " ComunicadorFactory ");
//        return (ComunicadorFactory) clazz.newInstance();
//    }
}