package cartao;

import mensagem.emissor.Emissor;
import mensagem.emissor.EmissorCreator;
import mensagem.receptor.Receptor;
import mensagem.receptor.ReceptorCreator;

public class VisaComunicadorFactory implements ComunicadorFactory {
    private EmissorCreator emissorCreator = new EmissorCreator();
    private ReceptorCreator receptorCreator = new ReceptorCreator();

    public Emissor createEmissor() {
        return this.emissorCreator.create(EmissorCreator.VISA);
    }

    public Receptor createReceptor() {
        return this.receptorCreator.create(ReceptorCreator.VISA);
    }

//    public ComunicadorFactory getComunicadorFactory(Cartao cartao) {
//        String bandeira = cartao.getBandeira();
//        Class clazz = Class.forName(bandeira + " ComunicadorFactory ");
//        return (ComunicadorFactory) clazz.newInstance();
//    }

}