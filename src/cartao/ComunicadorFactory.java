package cartao;

import mensagem.emissor.Emissor;
import mensagem.receptor.Receptor;

public interface ComunicadorFactory {
    Emissor createEmissor();
    Receptor createReceptor();
}
